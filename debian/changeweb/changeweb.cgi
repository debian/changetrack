#!/usr/bin/perl

$version = 0.02;
$this = "changeweb.cgi";

$path = "/var/lib/changetrack/";

@months =
("","January","February","March","April","May","June","July","August","September","October","November","December");
# couldn't be zero-based because I want the months to be all true.
# Also I think there's some other reason it didn't work.

use Text::ParseWords;
use Time::Local;

print "Content-type: text/html\n\n";

$| = 1;  # flush STDOUT

open(STDERR,">&STDOUT");
# let the web user see any errors.

$args = $ENV{QUERY_STRING};

# translate ampersands to semicolons, so we can be W3C compliant.
while($args =~ m/&/) { $args =~ s/&/;/; }

$args = $args . ";";
@ARGV = quotewords(";",0,$args); 
%things = {};

foreach $arg (@ARGV)
	{
	$arg = $arg . "=";
	($thing,$value) = quotewords("=",0,$arg);
	if(!$value) {$value=1;}
	$things{$thing} = $value;
	}

$year = $things{"year"}; if(!$year) {$year = 1999;}
$month= $things{"month"};if(!$month){$month= 1;}
$day  = $things{"day"};  if(!$day)  {$day  = 1;} elsif($day > 31) {$day = 31;}

$file = $things{"file"};
while($file =~ m/\.\./) {$file =~ s/\.\.//;}

$sort = $things{"sort"}; if(!$sort || $sort == 1) {$sort = "";} # deals with 'sort=' url problem

$date = timelocal(0,0,0,$day,$month-1,$year-1900); 

# feel free to add other groups of servers to monitor.
# note that each needs to have the same setup as the current ones;
# that should be cleaned up.

if($file)
	{
	$file =~ s/\.history//;	
	$title = $file;
	$file .= ".history";	
	while($title =~ m/:/) {$title =~ s/:/\//}
	$title = $title;
	}
else
	{
	$title = "Changetrack: $months[$month] $day, $year";
	}
print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
<html>
<head>
  <title>$title</title>
  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
	<h2>$title</h2>
  <FORM METHOD=GET action=\"$this\">
  "; 

print "<table border=0 cellspacing=0 cellpadding=4><tr><td>";
# needs to be a table to be in one line. 

print "
	Since:
	<select name=\"year\">
	<option value=$year selected>$year";

@i = localtime;

$currentyear = $i[5] + 1900;

if($file) {$localfile = "file=$file";}

for($i = 1999; $i <= $currentyear; $i++)
	{
	print "<option value=$i>$i";	
	}
print "</select>";	
print "</td><td>";
print "<table cellspacing=0 border=0 cellpadding=0><tr><td>";
	
# table to keep arrows in place 
	print "	
	<select name=\"month\">";

	for($i=1; $i <= 12; $i++)
		{	
		if($i == $month)
			{
			print "<option value=$i SELECTED>$months[$i]\n";

			}
		else
			{
			print "<option value=$i>$months[$i]\n";	
			}
		}
	print "</select>";
print "</td><td>";
print "<table cellspacing=0 border=0 cellpadding=0><tr><td>\n";
                        # table to put arrows above each other

if($month < 12)         # month 'up' arrow 
  {
	# not into next year; increment month
	print "<a href=\"$this?".
	    ($sort ? "sort=$sort;" : "").
		"day=$day;year=$year;$localfile;month=",$month+1,"\">";
  }
else
  {
 	# next year; month -> Jan 
	print "<a href=\"$this?".
	    ($sort ? "sort=$sort;" : "").
		"day=$day;year=",$year+1,";$localfile;month=1\">";
  }
#print "<img width=13 height=12 border=0 src=\"/netstat_dir_for_mrtg/graphics/plus.gif\">";
print "+";
print "</a>";
print "</td></tr><tr><td>";

if($month > 1)                                    # month 'down' arrow 
  {
	print "<a href=\"$this?".
	    ($sort ? "sort=$sort;" : "").
		"day=$day;year=$year;$localfile;month=",$month-1,"\">";
  }
else  
  {
	print "<a href=\"$this?".
	    ($sort ? "sort=$sort;" : "").
		"day=$day;year=",$year-1,";$localfile;month=12\">";
	}
#print "<img width=13 height=12 border=0 src=\"/netstat_dir_for_mrtg/graphics/minus.gif\">";
print "-";
print "</a>";
print "</td></tr></table>";
print "</td></tr></table>";

print "</td><td>";
print "<table><tr><td>";
print "<input name=day size=2 maxlength=2 value=\"$day\">";
print "</td><td>";
print "<table cellspacing=0 border=0 cellpadding=0><tr><td>";

@lens = (0,31,29,31,30,31,30,31,31,30,31,30,31); # Easiest to just assume Feb is always long.
$monlen = $lens[$month]; 
if($day < $monlen)
	{print "<a
href=\"$this?".
    ($sort ? "sort=$sort;" : "").
	"month=$month;year=$year;$localfile;day=",$day+1,"\">";}
else
	{
 	if($month < 12)
		{print "<a
href=\"$this?".
    ($sort ? "sort=$sort;" : "")
	."month=",$month+1,";year=$year;$localfile;day=1\">";}
	else
    {print "<a
href=\"$this?".
    ($sort ? "sort=$sort;" : "")
	."month=1;year=",$year+1,";$localfile;day=1\">";}
	}
#print "<img width=13 height=12 border=0 src=\"/netstat_dir_for_mrtg/graphics/plus.gif\">";
print "+";
print "</a>";
print "</td></tr><tr><td>";
if($day > 1)
	{print "<a
href=\"$this?".($sort ? "sort=$sort;" : ""). "month=$month;year=$year;$localfile;day=",$day-1,"\">";}
else
	{
	if($month > 1)
		{print "<a
href=\"$this?".($sort ? "sort=$sort;" : ""). "month=",$month-1,";year=$year;$localfile;day=",$lens[$month-1],"\">";}
	else
		{print "<a
href=\"$this?".($sort ? "sort=$sort;" : ""). "month=12;year=",$year-1,";$localfile;day=31\">";}
	}	
#print "<img width=13 height=12 border=0 src=\"/netstat_dir_for_mrtg/graphics/minus.gif\">";
print "-";
print "</a>";
print "</td></tr></table>";
print "</td></tr></table>";

print "</td><td>";
print "<input type=\"hidden\" name=\"sort\" value=\"$sort\">";
print "<input type=\"submit\" value=\"List Files\">";	
print "</td></tr></table>\n";

@i = stat "$path/etc:shutdownlog.history"; 
($i,$i,$i,$bootday,$bootmonth,$bootyear,$i,$i,$i) = localtime($i[9]);
$bootmonth++; $bootyear += 1900; 

print "<hr>";

if($file)
	{
	open(FILE,"$path/$file") or die "Can't open $path/$file:
$!\n";
	print "<pre>";
	$found = 0;
	if(($month >= 1) && ($month <= 12))
		{
		while(<FILE>)
	 		{
	    $line = $_;
	    if(substr($line,0,2) ne "  ")
	     	{
	     	$fyear = substr($line,36,4);
				$fmonth= substr($line,20,3);
				$fday  = substr($line,24,2);	

				if(!$fday || !$fmonth || !$fyear)
					{
					next;	
					}

				# this is for reading from the file.
				($fmonth =~ s/Jan/1/) || ($fmonth =~
s/Feb/2/) || ($fmonth =~ s/Mar/3/) ||
					($fmonth =~ s/Apr/4/) || ($fmonth =~
s/May/5/) || ($fmonth =~ s/Jun/6/) ||
	        ($fmonth =~ s/Jul/7/) || ($fmonth =~ s/Aug/8/) || ($fmonth
=~ s/Sep/9/) ||
	        ($fmonth =~ s/Oct/10/)|| ($fmonth =~ s/Nov/11/)|| ($fmonth
=~ s/Dec/12/);

				$fdate =
timelocal(0,0,0,$fday,$fmonth-1,$fyear-1900); 
				if($fdate >= $date)
					{
	        print $line;
	        $found++;
	        last;   
					}
				}
      }       
    }

	if(!$found) {print "No changes since that date.\n";}

	while(<FILE>)
    {
    print $_;       
    }
	close(FILE);
	print "</pre>"; 
	}
else
	{	
	print "<table bgcolor=\"#777777\" cellpadding=0 border=0
cellspacing=1>\n";
	print "<tr bgcolor=\"#e4e4e4\">\n";

	$selectedc = "#b0b0b0";

	if(!$sort)
		{
		print "<td bgcolor=\"$selectedc\">";
		print "<center>[--]";
		}
	else
		{
		print "<td><center>";
		print "<a
href=\"$this?day=$day;month=$month;year=$year\">[--]</a>
";
		}	
	print "</center></td>\n";
	
	if($sort eq "name")
		{
		print "<td bgcolor=\"$selectedc\">";
		print "<center>Filename";
		}
	else
		{
		print "<td>";
		print "<center><a
href=\"$this?day=$day;month=$month;year=$year;sort=name\">Filename</a>";
		}	
	print "</center></td>\n";
	
	if($sort eq "size")
		{
		print "<td bgcolor=\"$selectedc\">";
		print "<center>Log Size";
		}
	else
		{	
		print "<td>";
		print "<center><a
href=\"$this?day=$day;month=$month;year=$year;sort=size\">Log Size</a>";
		}	
	print "</center></td>\n";
	
	if($sort eq "date")
		{
		print "<td bgcolor=\"$selectedc\">";
		print "<center>Date Modified";
		}
	else
		{
		print "<td><center>";
		print "<a
href=\"$this?day=$day;month=$month;year=$year;sort=date\">Date Modified</a>";
		}	
	print "</center></td></tr>\n";	


	# There is no file, so show the directory listing.
	chdir("$path");
	open(LS,"ls *.history |") or die "Can't open ls $path: $!\n";

	$j = 1;
	@files = ();
	while(<LS>)
		{
		# list the files
		chomp;	
		$cname = $_;
		$hname = $_;
		@finfo = stat $cname;
		@fmdate = localtime($finfo[9]);
		$fyear = $fmdate[5] + 1900;
		$fmonth= $fmdate[4] + 1; 
		$fday  = $fmdate[3];
		$filesize = $finfo[7];

		$fdate = $finfo[9];
		$fdate = substr($fdate,0,10)." ".substr($fdate,-4,4);

		if(($month < 1) || ($month > 12)) {$fyear = 65535}
		#while($hname =~ m/:/) {$hname =~ s/:/\//;}
		$hname =~ s/.history$//;

		if($finfo[9] >= $date)
			{
			if($sort eq "size")
				{
				$line = "$filesize\t$cname\t$fdate\t$j\t";
				}
			elsif($sort eq "date")
				{
				$line = "$fdate\t$filesize\t$cname\t$j\t";

				}
			else
				{
				# name or invalid	
				$line = "$cname\t$filesize\t$fdate\t$j\t";

				}
			@files = (@files,$line);	
			}
		else
			{
			next;	
			# we can ignore this file.	
			}

		while($hname =~ m/:/) {$hname =~ s/:/\//;}
		$hname =~ s/\.history$//;
		$j++;	
		}

	if($sort)
		{
		if($sort eq "name")
			{@files = sort @files;}
		else
			{
			# descending numeric sort	
			# shows largest/newest files first.	
			sub numerically {$b <=> $a}	
			@files = sort numerically @files;	
			}
		}
	
	# extract everything from the list, and print it. 
	$j = 0;
	foreach $file (@files)
		{
		if($j%10 < 5)
			{
			$color = "#ffffff"; 	
			$hcolor= "#e4e4e4";	
			}
		else
			{
			$color = "#ddddff";
			$hcolor= "#ddddff";	
			}
		$j++;

		if($sort eq "size")
			{
			($fsize,$fname,$fdate,$fnum) =
quotewords("\t",0,$file);	
			}
		elsif($sort eq "date")
			{
			($fdate,$fsize,$fname,$fnum) =
quotewords("\t",0,$file);	
			}
		else
			{
			# name or invalid	
			($fname,$fsize,$fdate,$fnum) =
quotewords("\t",0,$file);
			}

		$fdate = scalar localtime($fdate);
		$fdate = substr($fdate,0,11) . substr($fdate,-4,4); 

		$hname = $fname;
		while($hname =~ m/:/) {$hname =~ s/:/\//;}
		$hname =~ s/\.history$//;
		$fname = "<a
href=\"$this?file=$fname;day=$day;month=$month;year=$year;sort=$sort\">$hname</a>";

		print "<tr bgcolor=\"$color\">";
		if($sort eq "name")
			{
		  print "<td>$fnum</td><td
bgcolor=\"$hcolor\">$fname</td><td>$fsize</td><td>$fdate</td>";  
			}
		elsif($sort eq "size")
			{
      print "<td>$fnum</td><td>$fname</td><td
bgcolor=\"$hcolor\">$fsize</td><td>$fdate</td>";  
			}
		elsif($sort eq "date")
			{
      print "<td>$fnum</td><td>$fname</td><td>$fsize</td><td
bgcolor=\"$hcolor\">$fdate</td>";  
			}
		else
			{
			print "<td
bgcolor=\"$hcolor\">$fnum</td><td>$fname</td><td>$fsize</td><td>$fdate</td>";
			}
		print "</tr>\n";
		}

	print "</table>";
	if($month > 0 && $month < 13) {print "$j files modified since
$months[$month] $day, $year.";}
	close(LS);
	}

print "</FORM>
<hr>
<table>
<tr><td>
<a href=\"http://validator.w3.org/check/referer\"><img border=\"0\"
        src=\"http://www.w3.org/Icons/valid-html401\"
        alt=\"Valid HTML 4.01!\" height=\"31\" width=\"88\"></a>

</td><td>
Copyright &copy; 2003 <a href=\"mailto:camero\@morland.ca\">Cameron Morland</a>
Verbatim copying and distribution of this entire article is permitted in any medium, provided this notice is preserved.
</td>
</tr>
</table>
";
